Slice Polygons
==============

Create a test polygon with a hole:

```
pol <- st_as_sf(data.frame(wkt="POLYGON((-180 -20, -140 55, 10 0, -140 -60, -180 -20),(-150 -20, -100 -10, -110 20, -150 -20))"),wkt="wkt")

plot(pol)
```

Create a test raster:


```
r= raster(extent(pol),nrow=100, ncol=100)
r[]=1:ncell(r)
plot(r)
plot(pol, add=TRUE)
```

Cut in half by population:

```
parts=chop_equal_raster(pol, r, c(1/2))
plot(parts)
```

Compute population under each half:

```
> masked_raster_sum(r, parts[1,])
[1] 11625428
> masked_raster_sum(r, parts[2,])
[1] 11727848
```

Note the difference because chopping vertically means whole columns
of the raster can only be in or out of the region, so the function 
that finds the cut point is discretised. The code could possibly 
be changed to find cut points that sliced individual raster cells
up but that would probably make it a lot slower.

The `chop_equal_raster` function can chop into any number of any fractions by
specifying the fraction between 0 and 1. So for example to split into 
equal *thirds* of population:

```
chop_equal_raster(pol, r, c(1/3, 2/3))
```


